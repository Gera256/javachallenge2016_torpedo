package hu.javaslat.torpedo.geometry;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import hu.javaslat.torpedo.model.Position;
import hu.javaslat.torpedo.model.geometry.Geometry;

public class GeometryTest{

	@Test
	public void lengthTest(){
				
		Position vector = new Position(1,0);
		
		assertEquals(1, Geometry.length(vector), 0);
		
	}
	

	@Test
	public void lengthTest1(){
				
		Position vector = new Position(-1,0);
		
		assertEquals(1, Geometry.length(vector), 0);
		
	}
	
	@Test
	public void lengthTest2(){
				
		Position vector = new Position(1.7454545451,0);
		
		assertEquals(1.7454545451, Geometry.length(vector), 0.001);
		
	}
	
	@Test
	public void radianToDegreeTest(){
		
		assertEquals(90, Geometry.radianToDegree(1.57), 0.1);
		
	}
	
	@Test
	public void radianToDegreeTest1(){
		
		assertEquals(-90, Geometry.radianToDegree(-1.57), 0.1);
		
	}
	
	@Test
	public void radianToDegreeTest2(){
		
		assertEquals(360, Geometry.radianToDegree(2*Geometry.PI), 0.1);
		
	}
	
	@Test
	public void degreeTest(){
		
		Position v0 = new Position(1,0);
		Position v1 = new Position(0,-1);
		double angle = Geometry.computeAngle(v0, v1);
		double degree = Geometry.radianToDegree(angle);
		
		assertEquals(90, degree, 0.1);		
		
	}
	
	@Test
	public void createVectorTest(){
		
		double angle = 45;
		Position vector = Geometry.angleToVector(angle);
		
		assertEquals(0.707, vector.getX(), 0.1);
		assertEquals(0.707, vector.getY(), 0.1);
		
	}
	
	@Test
	public void createVectorTest1(){
		
		double angle = 135;
		Position vector = Geometry.angleToVector(angle);
		
		assertEquals(-0.707, vector.getX(), 0.1);
		assertEquals(0.707, vector.getY(), 0.1);
		
	}
	
	@Test
	public void createVectorTest2(){
		
		double angle = 225;
		Position vector = Geometry.angleToVector(angle);
		
		assertEquals(-0.707, vector.getX(), 0.1);
		assertEquals(-0.707, vector.getY(), 0.1);
		
	}
	
	@Test
	public void createVectorTest3(){
		
		double angle = 315;
		Position vector = Geometry.angleToVector(angle);
		
		assertEquals(0.707, vector.getX(), 0.1);
		assertEquals(-0.707, vector.getY(), 0.1);
		
	}
	
	@Test
	public void crossProductTest(){
		
		Position v0 = new Position(1,0);
		Position v1 = new Position(1,0.1);
		
		assertEquals(0.1, Geometry.crossProduct(v0, v1), 0.1);
		
	}
	
	@Test
	public void lineCircleIntersectionTest(){
		
		Position p1 = new Position(-88,90);
		Position p2 = new Position(500,-250);
		
		Position c = new Position(0,0);
		double cRadius = 100;
		
		assertTrue(Geometry.isLineIntersectCircle(p1, p2, c, cRadius));
		
	}
}
