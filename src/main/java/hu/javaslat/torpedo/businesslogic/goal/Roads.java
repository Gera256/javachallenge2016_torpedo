package hu.javaslat.torpedo.businesslogic.goal;

import hu.javaslat.torpedo.model.Position;
import hu.javaslat.torpedo.model.Road;

public class Roads {

	
	public static final Road clockwise = new Road();
	public static final Road antiClockwise = new Road();
	public static final Road leftSide = new Road();
	public static final Road rightSide = new Road();
	
	static {

		clockwise.addPosition(new Position(850,650));
		clockwise.addPosition(new Position(1050,650));
		clockwise.addPosition(new Position(1050,150));
		clockwise.addPosition(new Position(150,150));
		clockwise.addPosition(new Position(150,650));

		antiClockwise.addPosition(new Position(450,150));
		antiClockwise.addPosition(new Position(850,150));
		antiClockwise.addPosition(new Position(1350,150));
		antiClockwise.addPosition(new Position(1350,650));
		antiClockwise.addPosition(new Position(450,650));
		
		leftSide.addPosition(new Position(500,650));
		leftSide.addPosition(new Position(150,650));
		leftSide.addPosition(new Position(150,150));
		leftSide.addPosition(new Position(500,150));

		rightSide.addPosition(new Position(1100,650));
		rightSide.addPosition(new Position(1100,150));
		rightSide.addPosition(new Position(1550,150));
		rightSide.addPosition(new Position(1550,650));
	}
	
}
