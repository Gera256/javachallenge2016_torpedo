package hu.javaslat.torpedo.model;

public class IslandPosition {

	private float x;
	private float y;
	
	public float getX() {
		return x;
	}
	
	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "IslandPosition [x=" + x + ", y=" + y + "]";
	}
	
	
	

}
