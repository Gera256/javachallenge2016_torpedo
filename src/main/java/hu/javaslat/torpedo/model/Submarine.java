package hu.javaslat.torpedo.model;

public class Submarine {

	protected String type;
	protected Long id;
	protected Position position;
	protected Owner owner;
	protected float velocity;
	protected float angle;
	protected int hp;
	protected int sonarCooldown;
	protected int torpedoCooldown;
	protected int sonarExtended;

	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Position getPosition() {
		return position;
	}
	
	public void setPosition(Position position) {
		this.position = position;
	}
	
	public Owner getOwner() {
		return owner;
	}
	
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	
	public float getVelocity() {
		return velocity;
	}
	
	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}
	
	public float getAngle() {
		return angle;
	}
	
	public void setAngle(float angle) {
		this.angle = angle;
	}
	
	public int getHp() {
		return hp;
	}
	
	public void setHp(int hp) {
		this.hp = hp;
	}
	
	public int getSonarCooldown() {
		return sonarCooldown;
	}
	
	public void setSonarCooldown(int sonarCooldown) {
		this.sonarCooldown = sonarCooldown;
	}
	
	public int getTorpedoCooldown() {
		return torpedoCooldown;
	}
	
	public void setTorpedoCooldown(int torpedoCooldown) {
		this.torpedoCooldown = torpedoCooldown;
	}
	
	public int getSonarExtended() {
		return sonarExtended;
	}
	
	public void setSonarExtended(int sonarExtended) {
		this.sonarExtended = sonarExtended;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Submarine other = (Submarine) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Submarine [type=" + type + ", id=" + id + ", position=" + position + ", owner=" + owner + ", velocity="
				+ velocity + ", angle=" + angle + ", hp=" + hp + ", sonarCooldown=" + sonarCooldown
				+ ", torpedoCooldown=" + torpedoCooldown + ", sonarExtended=" + sonarExtended + "]";
	}
	
}
