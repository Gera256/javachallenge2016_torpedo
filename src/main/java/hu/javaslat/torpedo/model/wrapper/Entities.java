package hu.javaslat.torpedo.model.wrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import hu.javaslat.torpedo.Config;
import hu.javaslat.torpedo.model.Entity;

public class Entities {
	
	private Map<Long,Set<Entity>> views = new HashMap<>();
	
	public void put(Long submarineId, List<Entity> entities){
		
		views.put(submarineId, new HashSet<Entity>(entities));
		
	}
	
	public Entity getEnemySubmarineById(Long enemySubmarineId){
		
		Entity enemy = null;
		
		for(Entity entity: getAllEntity()){
			
			if(enemySubmarineId.equals(entity.getId()))
				enemy = entity;
			
		}
		
		return enemy;
	}
	
	public List<Entity> getEnemySubmarineForSubmarine(Long submarineId){
		List<Entity> enemySubmarines = new ArrayList<>();

		for(Entity entity: views.get(submarineId)){

			if(entity.getType().equals(Config.EntityType.SUBMARINE.name)
					&& !entity.getOwner().getName().equals(Config.TEAM_NAME)){
				
				enemySubmarines.add(entity);
				
			}
				
			
		}
		
		
		return enemySubmarines;
		
	}
	
	public List<Entity> getAllEntity(){
		
		Set<Entity> result = new HashSet<>();

		for(Long submarineId : views.keySet()){
			
			result.addAll(views.get(submarineId));
			
		}
		
		return new ArrayList<>(result);
	}

        public Map<Long, Set<Entity>> getViews() {
            return views;
        }

}
