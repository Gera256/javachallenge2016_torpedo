package hu.javaslat.torpedo.model.wrapper;

import java.util.HashMap;
import java.util.Map;

import hu.javaslat.torpedo.model.Game;

public class GameContext {
	
	private Game game;
	private Map<Long, SimpleSubmarine> submarines = new HashMap<>();
	private Entities entities = new Entities();
	
	public Game getGame() {
		return game;
	}
	
	public void setGame(Game game) {
		this.game = game;
	}
	
	public Map<Long, SimpleSubmarine> getSubmarines() {
		return submarines;
	}

	public void setSubmarines(Map<Long, SimpleSubmarine> submarines) {
		this.submarines = submarines;
	}

	public Entities getEntities() {
		return entities;
	}
	
	public void setEntities(Entities entities) {
		this.entities = entities;
	}
	
}
