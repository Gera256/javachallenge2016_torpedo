package hu.javaslat.torpedo.model;

public class Shoot {
	
	private double angle;

	public Shoot(){
		
	}
	
	public Shoot(double angle) {
		super();
		this.angle = angle;
	}

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	@Override
	public String toString() {
		return "Shoot [angle=" + angle + "]";
	}
	
	

}
