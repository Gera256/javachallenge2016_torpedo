package hu.javaslat.torpedo.model;

public class Entity {

	private String type;
	private Long id;
	private Position position;
	private Owner owner;
	private float velocity;
	private float angle;
	private int roundsMoved;
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Position getPosition() {
		return position;
	}
	
	public void setPosition(Position position) {
		this.position = position;
	}
	
	public Owner getOwner() {
		return owner;
	}
	
	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	
	public float getVelocity() {
		return velocity;
	}
	
	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}
	
	public float getAngle() {
		return angle;
	}
	
	public void setAngle(float angle) {
		this.angle = angle;
	}
	
	public int getRoundsMoved() {
		return roundsMoved;
	}
	
	public void setRoundsMoved(int roundsMoved) {
		this.roundsMoved = roundsMoved;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entity other = (Entity) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Entity [type=" + type + ", id=" + id + ", position=" + position + ", owner=" + owner + ", velocity="
				+ velocity + ", angle=" + angle + ", roundsMoved=" + roundsMoved + "]";
	}

	
	
}
