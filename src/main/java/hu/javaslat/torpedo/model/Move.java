package hu.javaslat.torpedo.model;

public class Move {

	private double speed;
	private double turn;
	
	public double getSpeed() {
		return speed;
	}
	
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	
	public double getTurn() {
		return turn;
	}
	
	public void setTurn(double turn) {
		this.turn = turn;
	}

	@Override
	public String toString() {
		return "Move [speed=" + speed + ", turn=" + turn + "]";
	}
	
}
