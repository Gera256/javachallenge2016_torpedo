package hu.javaslat.torpedo.model;

public enum Action {

	CREATE_GAME, 
	LIST_GAMES, 
	JOIN_GAME, 
	GET_GAME_INFO, 
	GET_SUBMARINES, 
	MOVE, 
	SHOOT, 
	GET_SONAR, 
	EXTEND_SONAR;
	
}
