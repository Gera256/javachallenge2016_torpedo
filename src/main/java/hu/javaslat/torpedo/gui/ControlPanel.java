package hu.javaslat.torpedo.gui;


import java.util.ArrayList;
import java.util.List;

import hu.javaslat.torpedo.model.Submarine;
import javafx.geometry.Pos;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

@SuppressWarnings("restriction")
public class ControlPanel extends VBox {

	private GameControllerWithView controller;
	private List<SubmarinePanel> panels = new ArrayList<>();
	
	public ControlPanel(GameControllerWithView controller){
		
		this.controller = controller;
		
	}
	
	public void reset(){
		
		TabPane tabPane = new TabPane();
		int number = 1;
		
		for(Submarine submarine : controller.getGameContext().getSubmarines().values()){
			
			SubmarinePanel panel = new SubmarinePanel(controller);
			panel.setId(submarine.getId());
			
			panels.add(panel);
			
			HBox container = new HBox();
			container.getChildren().add(panel);
			container.setAlignment(Pos.BASELINE_CENTER);
			
			Tab tab = new Tab();
			tab.setText("Submarine - " + number++);
			tab.setContent(container);

			tabPane.getTabs().add(tab);
		}
		
		this.getChildren().add(tabPane);
		
	}
	
	public void refresh(){
		
		for(SubmarinePanel panel: panels){
			panel.refresh();
		}
		
	}
	
}
