package hu.javaslat.torpedo.gui;

import java.util.List;

import hu.javaslat.torpedo.Config;
import hu.javaslat.torpedo.GameController;
import hu.javaslat.torpedo.model.Entity;
import hu.javaslat.torpedo.model.Game;
import hu.javaslat.torpedo.model.IslandPosition;
import hu.javaslat.torpedo.model.Submarine;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

@SuppressWarnings("restriction")
public class TorpedoGUI extends Canvas {
	
	private GraphicsContext gc;
	private GameController controller;
	
	public TorpedoGUI(GameController controller){
		super(Config.GUI.GAME_MAP_WIDTH,Config.GUI.GAME_MAP_HEIGHT);

		this.controller = controller;
		gc = this.getGraphicsContext2D();
		 
		gc.setFill(Config.GUI.SEA_COLOR);
		gc.fillRect(0,0,Config.GUI.GAME_MAP_WIDTH,Config.GUI.GAME_MAP_HEIGHT);
	}
	
	public void refresh(){
		
		Game game = controller.getGameContext().getGame();

		gc.clearRect(0,0,Config.GUI.GAME_MAP_WIDTH,Config.GUI.GAME_MAP_HEIGHT);
		gc.setFill(Config.GUI.SEA_COLOR);
		
		gc.fillRect(0,0,Config.GUI.GAME_MAP_WIDTH,Config.GUI.GAME_MAP_HEIGHT);
		gc.setFill(Config.GUI.ISLAND_COLOR);
		
		IslandPosition iPos = game.getMapConfiguration().getIslandPositions().get(0);
		double islandSize = game.getMapConfiguration().getIslandSize();
		

		gc.translate(CoordinateXToPixel(iPos.getX()), CoordinateYToPixel(iPos.getY()));
		gc.fillOval(-islandSize/2, -islandSize/2, islandSize, islandSize);
		gc.translate(-CoordinateXToPixel(iPos.getX()), -CoordinateYToPixel(iPos.getY()));
		
		double sonarRadius = game.getMapConfiguration().getSonarRange();
		
		for(Submarine submarine: controller.getGameContext().getSubmarines().values()){
			
			if(submarine.getSonarExtended() > 0)
				sonarRadius = controller.getGameContext().getGame().getMapConfiguration().getExtendedSonarRange();
			
			drawSubmarine(submarine,Config.GUI.TEAM_1_SUBMARINE_COLOR, sonarRadius);
		}
		
		List<Entity> entities = controller.getGameContext().getEntities().getAllEntity();
		
		for(Entity entity : entities){
			if(entity.getType().equals(Config.EntityType.SUBMARINE.name)){
				if(!entity.getOwner().getName().equals(Config.TEAM_NAME)){
					drawSubmarine(entity, Config.GUI.TEAM_2_SUBMARINE_COLOR);
				}
			} else if(entity.getType().equals(Config.EntityType.TORPEDO.name)){
				drawTorpedo(entity, Config.GUI.TORPEDO_COLOR);
			}
		}
		
		
	}
	
	private void drawTorpedo(Entity entity, Color torpedoColor) {

		double torpedoX = CoordinateXToPixel(entity.getPosition().getX());
		double torpedoY = CoordinateYToPixel(entity.getPosition().getY());
		
		double radius = controller.getGameContext().getGame().getMapConfiguration().getTorpedoExplosionRadius();
		
		gc.setStroke(torpedoColor);
		gc.setFill(torpedoColor);
		gc.strokeOval(torpedoX-radius/2, torpedoY-radius/2, radius, radius);
		gc.fillOval(torpedoX-Config.GUI.TORPEDO_SIZE/2, torpedoY-Config.GUI.TORPEDO_SIZE/2, Config.GUI.TORPEDO_SIZE, Config.GUI.TORPEDO_SIZE);
		
	}

	private void drawSubmarine(Entity entity, Color team2SubmarineColor) {
		Submarine submarine = new Submarine();
		submarine.setPosition(entity.getPosition());
		submarine.setAngle(entity.getAngle());
		
		drawSubmarine(submarine,team2SubmarineColor, 0);
	}

	private void drawSubmarine(Submarine submarine, Color color, double sonarRadius){
		Game game = controller.getGameContext().getGame();
		double submarineSize = game.getMapConfiguration().getSubmarineSize();
		double submarineCenterX = CoordinateXToPixel(submarine.getPosition().getX());
		double submarineCenterY = CoordinateYToPixel(submarine.getPosition().getY());

		gc.translate(submarineCenterX, submarineCenterY);
		gc.rotate(translateAngle(submarine.getAngle()));
		gc.setStroke(color);
		
		//Tengeralattjáró méretének kirajzolása
		gc.strokeOval(-submarineSize/2, -submarineSize/2, submarineSize, submarineSize);
		
		gc.strokeOval(-sonarRadius/2, -sonarRadius/2, sonarRadius, sonarRadius);
		gc.strokePolyline(new double[]{-submarineSize/2 , 0 , submarineSize/2},
				new double[]{0, -submarineSize/2, 0}, 
				3);
		gc.rotate(-translateAngle(submarine.getAngle()));
		gc.translate(-submarineCenterX, -submarineCenterY);
		
	}
	
	private double translateAngle(double a){
		
		if(a>=0 && a<=90){
			return 90-a;
		} else if(a > 90 && a <= 180){
			return 360-(a-90);
		} else if(a > 180 && a <= 270){
			return 270-(a-180);
		} else {
			return 180-(a-270);
		}
		
	}
	
	private double CoordinateXToPixel(double p){
		return p/Config.GUI.GAME_MAP_REAL_SIZE_TO_CANVAS;
	}
	
	private double CoordinateYToPixel(double p){
		return Config.GUI.GAME_MAP_HEIGHT - ( p/Config.GUI.GAME_MAP_REAL_SIZE_TO_CANVAS );
	}
}
