package hu.javaslat.torpedo.gui;

import hu.javaslat.torpedo.Config;
import hu.javaslat.torpedo.model.Game;
import hu.javaslat.torpedo.model.Move;
import hu.javaslat.torpedo.model.Shoot;
import hu.javaslat.torpedo.model.Submarine;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

@SuppressWarnings("restriction")
public class SubmarinePanel extends GridPane{

	private Long submarineId = 0L;
	private GameControllerWithView controller;
	private Label idLabel = new Label("id: " + 0);
	private Label hpLabel = new Label("hp: ");
	ProgressBar hpBar = new ProgressBar(1);
	private Label speedLabel = new Label("speed: " + 0);
	private Label angleLabel = new Label("angle: " + 0);
	private Label positionLabel = new Label("x: " + 200 + " y: " + 200);
	private Label speedInputLabel = new Label("speed: ");
	private TextField speedInput = new TextField();
	private Label turnInputLabel = new Label("turn: ");
	private TextField turnInput = new TextField();
	private Button moveButton = new Button("Move!");
	private Button fireButton = new Button("Fire!");
	private Label angleInputLabel = new Label("angle: ");
	private TextField angleInput = new TextField();
	
	
	public SubmarinePanel(GameControllerWithView controller){
		
		this.controller = controller;
		
		moveButton.setOnAction(e -> {
			Move move = new Move();
			move.setSpeed(Float.parseFloat(speedInput.getText()));
			move.setTurn(Float.parseFloat(turnInput.getText()));
			
			Game game = controller.getGameContext().getGame();
			
			controller.move(game.getId(), submarineId, move);
		});
		
		fireButton.setOnAction(e -> {
			Shoot shoot = new Shoot(Float.parseFloat(angleInput.getText()));
			
			Game game = controller.getGameContext().getGame();
			
			controller.fire(game.getId(), submarineId, shoot);
		});

		HBox hpBox = new HBox();
		hpBox.getChildren().addAll(hpLabel, hpBar);
		hpBox.setAlignment(Pos.CENTER);
		
		HBox speedInputBox = new HBox();
		speedInputBox.getChildren().addAll(speedInputLabel, speedInput);
		
		HBox turnInputBox = new HBox();
		turnInputBox.getChildren().addAll(turnInputLabel, turnInput);
		
		HBox angleInputBox = new HBox();
		angleInputBox.getChildren().addAll(angleInputLabel, angleInput);
		
	    this.add(idLabel, 0, 0, 2, 1);
	    this.add(hpBox, 0, 1, 2, 1);
	    this.add(speedLabel, 0, 2, 2, 1);
	    this.add(angleLabel, 0, 3, 2, 1);
	    this.add(positionLabel, 0, 4, 2, 1);
	    this.add(speedInputLabel, 0, 5, 1, 1);
	    this.add(speedInput, 1, 5, 1, 1);
	    this.add(turnInputLabel, 0, 6, 1, 1);
	    this.add(turnInput, 1, 6, 1, 1);
	    this.add(moveButton, 0, 7, 2, 1);
	    this.add(angleInputLabel, 0, 8, 1, 1);
	    this.add(angleInput, 1, 8, 1, 1);
	    this.add(fireButton, 0, 9, 2, 1);
	    
	    GridPane.setHalignment(idLabel, HPos.CENTER);
	    GridPane.setHalignment(speedLabel, HPos.CENTER);
	    GridPane.setHalignment(angleLabel, HPos.CENTER);
	    GridPane.setHalignment(positionLabel, HPos.CENTER);
	    GridPane.setHalignment(speedInputLabel, HPos.CENTER);
	    GridPane.setHalignment(turnInputLabel, HPos.CENTER);
	    GridPane.setHalignment(angleInputLabel, HPos.CENTER);
	    GridPane.setHalignment(moveButton, HPos.CENTER);
	    GridPane.setHalignment(fireButton, HPos.CENTER);
	    
	    this.setVgap(Config.GUI.CONTROL_PANEL_SPACING);
		
	}
	
	public void setId(Long submarineId){
		this.submarineId = submarineId;
		idLabel.setText("id: " + submarineId);
	}
	
	public void refresh(){
		
		Submarine currentSubmarine = new Submarine();
		
		for(Submarine submarine : controller.getGameContext().getSubmarines().values()){
			if(submarineId.equals(submarine.getId()))
				currentSubmarine = submarine;
		}

                if(currentSubmarine != null && currentSubmarine.getPosition() != null){
                    hpBar.setProgress((double)currentSubmarine.getHp()/100);		
                    speedLabel.setText("speed: " + currentSubmarine.getVelocity());
                    angleLabel.setText("angle: " + currentSubmarine.getAngle());
                    positionLabel.setText("x: " + currentSubmarine.getPosition().getX() + " y: " + currentSubmarine.getPosition().getY());
                }
		
	}
	
}
