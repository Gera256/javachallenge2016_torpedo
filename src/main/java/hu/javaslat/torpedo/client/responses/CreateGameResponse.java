package hu.javaslat.torpedo.client.responses;

public class CreateGameResponse {

	private String message;
	private int code;
	private long id;
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "CreateGameResponse [message=" + message + ", code=" + code + ", id=" + id + "]";
	}
	
	
	
}
