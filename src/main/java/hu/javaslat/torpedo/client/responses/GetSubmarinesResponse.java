package hu.javaslat.torpedo.client.responses;

import java.util.List;

import hu.javaslat.torpedo.model.Submarine;

public class GetSubmarinesResponse {

	private List<Submarine> submarines;
	private String message;
	private int code;
	
	public List<Submarine> getSubmarines() {
		return submarines;
	}
	
	public void setSubmarines(List<Submarine> submarines) {
		this.submarines = submarines;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
}
