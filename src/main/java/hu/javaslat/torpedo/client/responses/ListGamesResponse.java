package hu.javaslat.torpedo.client.responses;

import java.util.List;

public class ListGamesResponse {
	
	private List<Long> games;
	private String message;
	private int code;
	
	public List<Long> getGames() {
		return games;
	}
	
	public void setGames(List<Long> games) {
		this.games = games;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	@Override
	public String toString() {
		return "ListGamesResponse [games=" + games + ", message=" + message + ", code=" + code + "]";
	}

}
