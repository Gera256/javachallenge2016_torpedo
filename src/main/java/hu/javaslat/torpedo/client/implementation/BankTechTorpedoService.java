package hu.javaslat.torpedo.client.implementation;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;

import org.glassfish.jersey.client.ClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import hu.javaslat.torpedo.Config;
import hu.javaslat.torpedo.client.responses.CreateGameResponse;
import hu.javaslat.torpedo.client.responses.GetGameInfoResponse;
import hu.javaslat.torpedo.client.responses.GetSubmarinesResponse;
import hu.javaslat.torpedo.client.responses.JoinGameResponse;
import hu.javaslat.torpedo.client.responses.ListGamesResponse;
import hu.javaslat.torpedo.client.responses.MoveResponse;
import hu.javaslat.torpedo.client.responses.ShootResponse;
import hu.javaslat.torpedo.client.responses.UseActiveSonarResponse;
import hu.javaslat.torpedo.client.responses.UsePassiveSonarResponse;
import hu.javaslat.torpedo.interfaces.TorpedoService;
import hu.javaslat.torpedo.model.Move;
import hu.javaslat.torpedo.model.Shoot;

public class BankTechTorpedoService implements TorpedoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BankTechTorpedoService.class);
	private Client client;
	//private InvokeGuard invokeGuard;

	public BankTechTorpedoService(){
		ClientConfig cc = new ClientConfig();
		cc.register(JacksonJsonProvider.class);
		client = ClientBuilder.newClient(cc);
		client.register(new AuthenticationRequestFilter());
		client.register(new LoggingFilter());
	//	invokeGuard = new InvokeGuard();
        
	}
	
	public synchronized CreateGameResponse createGame() {
        WebTarget target = client.target(Config.createGameUrl());
        
        //invokeGuard.doAction(Action.CREATE_GAME);
        
        return target.request().post(null, CreateGameResponse.class);
	}

	public synchronized ListGamesResponse listGames() {
		WebTarget target = client.target(Config.listGameUrl());
        
        //invokeGuard.doAction(Action.LIST_GAMES);
        
        return target.request().get(ListGamesResponse.class);
	}

	public synchronized JoinGameResponse joinGame(long gameId) {
		WebTarget target = client.target(Config.joinGameUrl(gameId));
        
        //invokeGuard.doAction(Action.JOIN_GAME);
        
        return target.request().post(null,JoinGameResponse.class);
	}

	public synchronized GetGameInfoResponse getGameInfo(long gameId) {
		WebTarget target = client.target(Config.getGameInfoUrl(gameId));
        
        //invokeGuard.doAction(Action.GET_GAME_INFO);
        
        GetGameInfoResponse res = target.request().get(GetGameInfoResponse.class);
        
        //invokeGuard.setMinDuration(res.getGame().getMapConfiguration().getRoundLength());
        
        return res;
	}

	public synchronized GetSubmarinesResponse getSubmarines(long gameId) {
		WebTarget target = client.target(Config.getSubmarinesUrl(gameId));
        
        //invokeGuard.doAction(Action.GET_SUBMARINES);
        
        return target.request().get(GetSubmarinesResponse.class);
	}

	public synchronized MoveResponse move(long gameId, long submarineId, Move move) {
		WebTarget target = client.target(Config.getMoveUrl(gameId,submarineId));
        
        //invokeGuard.doAction(Action.MOVE);
        
        return target.request().post(Entity.json(move), MoveResponse.class);
	}

	public synchronized ShootResponse shoot(long gameId, long submarineId, Shoot shoot) {
		WebTarget target = client.target(Config.getShootUrl(gameId,submarineId));

        //invokeGuard.doAction(Action.SHOOT);
        
        return target.request().post(Entity.json(shoot), ShootResponse.class);
	}

	public synchronized UsePassiveSonarResponse usePassiveSonar(long gameId, long submarineId) {
		WebTarget target = client.target(Config.getPassiveSonarUrl(gameId,submarineId));
        
        //invokeGuard.doAction(Action.GET_SONAR);
        
        return target.request().get(UsePassiveSonarResponse.class);
	}

	public synchronized UseActiveSonarResponse useActiveSonar(long gameId, long submarineId) {
		WebTarget target = client.target(Config.getActiveSonarUrl(gameId,submarineId));

        //invokeGuard.doAction(Action.EXTEND_SONAR);
        
        return target.request().post(null , UseActiveSonarResponse.class);
	}

}
