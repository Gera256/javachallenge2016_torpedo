package hu.javaslat.torpedo.client.implementation;

public class TooManyInvokationException extends Exception {

	private static final long serialVersionUID = 7559925714151506207L;

	public TooManyInvokationException(String message) {
		super(message);
	}

	
	
}
