package hu.javaslat.torpedo.client.implementation;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map.Entry;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingFilter implements ClientRequestFilter,ClientResponseFilter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LoggingFilter.class);

	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
				
		LOGGER.info("Request url: {}", requestContext.getUri());
		
		LOGGER.info("Request headers");
		
		for(Entry<String,List<Object>> entry: requestContext.getHeaders().entrySet()){
			LOGGER.info("Header name: {}", entry.getKey());
			for(Object value : entry.getValue()){
				LOGGER.info("value: {}", value);
			}
		}
		
		LOGGER.info("Client request: {}", requestContext.getEntity());
	}

	@Override
	public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
		LOGGER.info("response");
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(responseContext.getEntityStream()));
		String line = reader.readLine();
		LOGGER.info(line);
		
		responseContext.setEntityStream(new ByteArrayInputStream(line.getBytes()));
	}

}
