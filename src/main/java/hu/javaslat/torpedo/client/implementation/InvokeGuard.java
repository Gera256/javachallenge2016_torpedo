package hu.javaslat.torpedo.client.implementation;

import hu.javaslat.torpedo.model.Action;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InvokeGuard {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InvokeGuard.class);
	private Map<Action, Integer> remainedInvokation;
	private Map<Action, Long> lastInvoke;
	private long minDuration;
	
	public InvokeGuard(){
		remainedInvokation = new HashMap<>();
		
		resetRemainedInvokations();
		
		lastInvoke = new HashMap<>();
		
	}

	public long getMinDuration() {
		return minDuration;
	}

	public void setMinDuration(long minDuration) {
		this.minDuration = minDuration;
	}
	
	public void doAction(Action action){
		long elapsedTime;
		long currentTime;
		
		try {
			consumeRemainedAction(action);
		} catch (TooManyInvokationException e1) {
			
			currentTime = System.currentTimeMillis();
			elapsedTime = currentTime - lastInvoke.get(action);
			
			LOGGER.warn(e1.getMessage());
			LOGGER.warn("currentTime: " + currentTime);
			LOGGER.warn("elapsedTime: " + elapsedTime);
			LOGGER.warn("minDuration: " + minDuration);
			
			if(elapsedTime < minDuration){
				try {
					Thread.sleep(minDuration - elapsedTime + 50);
					resetRemainedInvokations();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		}			
		
	}
	
	private void consumeRemainedAction(Action action) throws TooManyInvokationException{
		
		if(remainedInvokation.containsKey(action)){
			Integer remainedAction = remainedInvokation.get(action);

			LOGGER.info("ConsumeAction: " + action);
			LOGGER.info("Remained number: " + remainedAction);
			
			if(remainedAction == -1){
				return;
			} else if(remainedAction == 0){
				lastInvoke.put(action, System.currentTimeMillis());
				throw new TooManyInvokationException("Too many invokation: " + action);
			}
			else{
				lastInvoke.put(action, System.currentTimeMillis());
				remainedInvokation.put(action, --remainedAction);
			}
			
		} else {
			LOGGER.warn("Action not contained in remainedInvokation: " + action);
		}
		
	}
	
	private void resetRemainedInvokations(){

		remainedInvokation.put(Action.CREATE_GAME, -1);
		remainedInvokation.put(Action.LIST_GAMES, -1);
		remainedInvokation.put(Action.JOIN_GAME, -1);
		remainedInvokation.put(Action.GET_GAME_INFO, -1);
		remainedInvokation.put(Action.GET_SUBMARINES, -1);
		remainedInvokation.put(Action.MOVE, 2);
		remainedInvokation.put(Action.SHOOT, 2);
		remainedInvokation.put(Action.GET_SONAR, 2);
		remainedInvokation.put(Action.EXTEND_SONAR, 2);
		
	}

}
