package hu.javaslat.torpedo;

import javafx.scene.paint.Color;

@SuppressWarnings("restriction")
public class Config {
	
	public static String SERVER;
	public static String PORT;
	public static String CONTEXT;
	
	public static String BASE_URL;
	
	public static String TEAM_TOKEN_NAME;
	public static String TEAM_TOKEN_VALUE;
	
	public static String TEAM_NAME;
	
	public static double allowedDistance = 75;
        
        public static void init(String server, String port) {
            SERVER = server;
            PORT = port;
            CONTEXT = "jc16-srv";
            BASE_URL = "http://" + SERVER + ":" + PORT + "/" + CONTEXT + "/";
            TEAM_TOKEN_NAME = "TEAMTOKEN";
            TEAM_TOKEN_VALUE = "1389EAB9B724B8A38266EC3D2114ADF3";
            TEAM_NAME = "Javaslat";
            allowedDistance = 75;
        }
	
	public enum EntityType {
		
		TORPEDO("Torpedo"), SUBMARINE("Submarine");
		
		public String name;
		
		private EntityType(String name){
			this.name = name;
		}
		
	}

	public static class GUI {
		
		public static final double DASHBOARD_SPACING = 10;
		public static final double GAME_LIST_SPACING = 10;
		public static final double CONTROL_PANEL_SPACING = 10;
		public static final double BUTTON_WIDTH = 80;
		
		public static final int GAME_MAP_WIDTH = 850;
		public static final int GAME_MAP_HEIGHT = 400;
		
		public static final int SIDE_PANEL_WIDTH = 250;
		
		//Az általam használt canvas fele akkora mint a valós játéké
		public static final float GAME_MAP_REAL_SIZE_TO_CANVAS = 2;
		
		public static final float TORPEDO_SIZE = 3;
		
		public static final Color SEA_COLOR = Color.rgb(23, 79, 168, 1);
		public static final Color ISLAND_COLOR = Color.rgb(155, 57, 0);
		
		public static final Color TEAM_1_SUBMARINE_COLOR = Color.rgb(0, 0, 0);
		public static final Color TEAM_2_SUBMARINE_COLOR = Color.rgb(255, 80, 0);

		public static final Color TORPEDO_COLOR = Color.rgb(255, 0, 0);
		
	}
	
	public static String createGameUrl(){
		return BASE_URL + "game";
	}
	
	public static String listGameUrl(){
		return BASE_URL + "game";
	}
	
	public static String joinGameUrl(long gameId){
		return BASE_URL + "game" + "/" + gameId;
	}
	
	public static String getGameInfoUrl(long gameId){
		return BASE_URL + "game" + "/" + gameId;
	}

	public static String getSubmarinesUrl(long gameId){
		return BASE_URL + "game" + "/" + gameId + "/submarine";
	}

	public static String getMoveUrl(long gameId, long submarineId){
		return BASE_URL + "game" + "/" + gameId + "/submarine/" + submarineId + "/move";
	}
	
	public static String getShootUrl(long gameId, long submarineId){
		return BASE_URL + "game" + "/" + gameId + "/submarine/" + submarineId + "/shoot";
	}

	public static String getPassiveSonarUrl(long gameId, long submarineId){
		return BASE_URL + "game" + "/" + gameId + "/submarine/" + submarineId + "/sonar";
	}
	
	public static String getActiveSonarUrl(long gameId, long submarineId){
		return BASE_URL + "game" + "/" + gameId + "/submarine/" + submarineId + "/sonar";
	}
	
}
