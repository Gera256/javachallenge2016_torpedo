package hu.javaslat.torpedo.interfaces;

import hu.javaslat.torpedo.client.responses.CreateGameResponse;
import hu.javaslat.torpedo.client.responses.GetGameInfoResponse;
import hu.javaslat.torpedo.client.responses.GetSubmarinesResponse;
import hu.javaslat.torpedo.client.responses.JoinGameResponse;
import hu.javaslat.torpedo.client.responses.ListGamesResponse;
import hu.javaslat.torpedo.client.responses.MoveResponse;
import hu.javaslat.torpedo.client.responses.ShootResponse;
import hu.javaslat.torpedo.client.responses.UseActiveSonarResponse;
import hu.javaslat.torpedo.client.responses.UsePassiveSonarResponse;
import hu.javaslat.torpedo.model.Move;
import hu.javaslat.torpedo.model.Shoot;

public interface TorpedoService {

	CreateGameResponse createGame();
	ListGamesResponse listGames();
	JoinGameResponse joinGame(long gameId);
	GetGameInfoResponse getGameInfo(long gameId);
	GetSubmarinesResponse getSubmarines(long gameId);
	MoveResponse move(long gameId, long submarineId, Move move);
	ShootResponse shoot(long gameId, long submarineId, Shoot shoot);
	UsePassiveSonarResponse usePassiveSonar(long gameId, long submarineId);
	UseActiveSonarResponse useActiveSonar(long gameId, long submarineId);
	
	
}
